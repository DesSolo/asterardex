from peewee import *
from config import config

db = SqliteDatabase(config.database)


class BaseModel(Model):
    class Meta:
        database = db


class Channel(BaseModel):
    CallerIDNum = CharField()
    Exten = CharField()
    Uniqueid = CharField()
    Linkedid = CharField()


def create_tables():
    Channel.create_table()


def drop_tables():
    Channel.drop_table()


if __name__ == '__main__':
    #channel = Channel.create(Uniqueiu=2)
    drop_tables()
    create_tables()
