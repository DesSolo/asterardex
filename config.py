import yaml


class Config(object):
    raw_data = {}
    is_edit = False

    def __init__(self, file_name):
        self.file_name = file_name
        self.reload()

    def reload(self):
        with open(self.file_name) as source_config:
            self.raw_data.update(**yaml.load(source_config.read()))

    def save(self):
        with open(self.file_name, 'w') as file:
            stream = yaml.dump(self.raw_data, default_flow_style=False, allow_unicode=True)
            file.write(stream)

    def __getattr__(self, item):
        return self.raw_data.get(item)

    def __getitem__(self, item):
        return self.__getattr__(item)

    def __setattr__(self, key, value):
        if key in ['file_name', 'is_edit']:
            super(Config, self).__setattr__(key, value)
        else:
            self.raw_data[key] = value
            self.is_edit = True

    def __setitem__(self, key, value):
        self.__setattr__(key, value)

    def __repr__(self):
        return repr(self.raw_data)


config = Config('config.yml')