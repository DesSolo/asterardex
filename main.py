from asterisk.ami import AMIClient
from config import config
import time
from models import Channel

client = AMIClient(**config.ami)
client.login(**config.login)


def event_listener(event, **kwargs):
    print(event)
    if event.name == 'Newchannel':
        channel = Channel.create(**event['keys'])

    # print('CHANNEL WRITED!!!', channel)


client.add_event_listener(event_listener)

try:
    while True:
        time.sleep(config.sleep)
except (KeyboardInterrupt, SystemExit):
    client.logoff()
